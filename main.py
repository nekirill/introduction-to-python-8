from HTMLParser import HTMLParser
from collections import defaultdict
import urllib2
import sqlite3

occurrences = defaultdict(int)
response = urllib2.urlopen('http://python.org/')
html = response.read()

class print_tags(HTMLParser):
    def handle_starttag(self, tag, attrs):
        occurrences[tag] += 1

parser = print_tags()
parser.feed(html)

def data_entry(data):
    for key, value in data.iteritems():
        c.execute("INSERT INTO htmltags(tag,occurrences) VALUES(?,?)", (str(key),value))
    conn.commit()

conn = sqlite3.connect(':memory:')

c = conn.cursor()
c.execute('''CREATE TABLE htmltags
             (id INTEGER PRIMARY KEY, tag text, occurrences int)''')

data_entry(occurrences)

c.execute("SELECT * FROM htmltags")
rows = c.fetchall()
for row in rows:
    print(row)

conn.close()

